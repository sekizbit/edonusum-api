<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI\Enums;

class Currency
{
    public const TRY = 1;
    public const USD = 2;
    public const EUR = 3;
    public const GBP = 4;
}
