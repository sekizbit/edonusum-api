<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI\Enums;

class InvoiceSubType
{
    public const SATIS         = 1;
    public const IADE          = 2;
    public const TEVKIFAT      = 3;
    public const ISTISNA       = 4;
    public const OZELMATRAH    = 5;
    public const IHRACKAYITLI  = 6;
    public const SEVK          = 7;
    public const SGK           = 8;
    public const MATBUDAN      = 9;
    public const KOMISYONCU    = 10;
    public const HKSSATIS      = 11;
    public const HKSKOMISYONCU = 12;
}
