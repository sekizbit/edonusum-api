<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI\Enums;

class InvoiceStatus
{
    public const DRAFT    = 1;
    public const CANCELED = 2;
}
