<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI\Enums;

class InvoiceDocumentType
{
    public const INVOICE   = 1;
    public const SEVOUCHER = 2;
}
