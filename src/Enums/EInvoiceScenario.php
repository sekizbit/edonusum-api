<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI\Enums;

class EInvoiceScenario
{
    public const TEMELFATURA        = 1;
    public const TICARIFATURA       = 2;
    public const YOLCUBERABERFATURA = 3;
    public const IHRACAT            = 4;
    public const TEMELIRSALIYE      = 5;
    public const EARSIVFATURA       = 6;
    public const EARSIVBELGE        = 7;
    public const OZELFATURA         = 8;
    public const KAMU               = 9;
    public const HKS                = 10;
}
