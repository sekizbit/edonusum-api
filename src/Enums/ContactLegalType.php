<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI\Enums;

class ContactLegalType
{
    public const CORPORATION = 1;
    public const PERSON      = 2;
}
