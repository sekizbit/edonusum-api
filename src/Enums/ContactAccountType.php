<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI\Enums;

class ContactAccountType
{
    public const CUSTOMER = 1;
    public const SUPPLIER = 2;
}
