<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI\Responses;

class GuzzleResponse implements ResponseInterface
{
    /**
     * @param $response
     * @return mixed
     */
    public function respond($response)
    {
        return json_decode($response->getBody()->getContents(), true);
    }
}
