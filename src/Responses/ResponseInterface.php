<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI\Responses;

interface ResponseInterface
{
    public function respond($response);
}
