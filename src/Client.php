<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI;

use GuzzleHttp\Client as RequestClient;
use GuzzleHttp\ClientInterface;
use Sekizbit\EDonusumAPI\Requests\Auth;
use Sekizbit\EDonusumAPI\Requests\Contacts;
use Sekizbit\EDonusumAPI\Requests\Products;
use Sekizbit\EDonusumAPI\Requests\SalesInvoices;

class Client
{
    public const API_BASE      = 'https://edonusum.sekizbit.com.tr/api/v1';
    public const API_BASE_TEST = 'https://test.edonusum.sekizbit.com.tr/api/v1';

    /**
     * @var \Sekizbit\EDonusumAPI\Client|null
     */
    private static $instance = null;

    /**
     * @var array
     */
    private $config;

    /**
     * @var \GuzzleHttp\ClientInterface
     */
    public $requestClient;

    /**
     * @param  array                             $config
     * @param  \GuzzleHttp\ClientInterface|null  $requestClient
     */
    public function __construct(array $config = [], ClientInterface $requestClient = null)
    {
        $this->config = $config;

        $this->prepareConfig($config);
        $this->prepareRequestClient($requestClient);
    }

    /**
     * @param  array  $config
     * @return \Sekizbit\EDonusumAPI\Client
     */
    public static function instance(array $config = []): ?Client
    {
        if (self::$instance === null) {
            self::$instance = new Client($config);
        }

        return self::$instance;
    }

    /**
     * @param  array  $config
     */
    protected function prepareConfig(array $config): void
    {
        $this->config = array_merge([
            'api_base' => $this->isTestMode() ? self::API_BASE_TEST : self::API_BASE,
        ], $config);
    }

    /**
     * @param  \GuzzleHttp\ClientInterface|null  $requestClient
     */
    protected function prepareRequestClient(ClientInterface $requestClient = null): void
    {
        if ($requestClient) {
            $this->requestClient = $requestClient;

            return;
        }

        $config = array_merge([
            'base_uri' => $this->config['api_base'] . '/' . $this->config['company'] . '/',
            'headers'  => [
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer ' . $this->config['api_token'],
            ],
        ], $this->config['client_config'] ?? []);

        $this->requestClient = new RequestClient($config);
    }

    /**
     * @return bool
     */
    public function isTestMode(): bool
    {
        if (isset($this->config['test_mode']) && $this->config['test_mode']) {
            return true;
        }

        return false;
    }

    /**
     * @return \Sekizbit\EDonusumAPI\Requests\Auth
     */
    public function auth(): Auth
    {
        return (new Auth($this->requestClient));
    }

    /**
     * @return \Sekizbit\EDonusumAPI\Requests\Contacts
     */
    public function contacts(): Contacts
    {
        return (new Contacts($this->requestClient));
    }

    /**
     * @return \Sekizbit\EDonusumAPI\Requests\Products
     */
    public function products(): Products
    {
        return (new Products($this->requestClient));
    }

    /**
     * @return \Sekizbit\EDonusumAPI\Requests\SalesInvoices
     */
    public function salesInvoices(): SalesInvoices
    {
        return (new SalesInvoices($this->requestClient));
    }
}
