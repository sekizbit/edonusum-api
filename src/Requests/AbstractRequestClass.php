<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI\Requests;

use Exception;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use Sekizbit\EDonusumAPI\Responses\GuzzleResponse;

class AbstractRequestClass
{
    /**
     * @var \GuzzleHttp\ClientInterface
     */
    protected $requestClient;

    /**
     * @param  \GuzzleHttp\ClientInterface  $requestClient
     */
    public function __construct(ClientInterface $requestClient)
    {
        $this->requestClient = $requestClient;
    }

    /**
     * @param  string  $method
     * @param          $uri
     * @param  array   $options
     * @return mixed|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request(string $method, $uri, array $options = [])
    {
        try {
            $response = $this->requestClient->request($method, $uri, $options);
            if ($response instanceof Response) {
                return (new GuzzleResponse())->respond($response);
            }

            return $response;
        } catch (Exception $e) {
            if ($e->getResponse() instanceof Response) {
                return (new GuzzleResponse())->respond($e->getResponse());
            }

            return $e->getMessage();
        }
    }

    /**
     * @param  array  $options
     * @return mixed|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index(array $options = [])
    {
        return $this->request('get', $this->path, $options);
    }

    /**
     * @param  int    $id
     * @param  array  $options
     * @return mixed|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function show(int $id, array $options = [])
    {
        return $this->request('get', $this->getShowPath($id), $options);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function store(array $options = [])
    {
        return $this->request('post', $this->path, $options);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function update(int $id, array $options = [])
    {
        return $this->request('put', $this->getUpdatePath($id), $options);
    }

    /**
     * @param  int  $id
     * @return string
     */
    public function getShowPath(int $id): string
    {
        return $this->path . '/' . $id;
    }

    /**
     * @param  int  $id
     * @return string
     */
    public function getUpdatePath(int $id): string
    {
        return $this->path . '/' . $id;
    }
}
