<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI\Requests;

class SalesInvoices extends AbstractRequestClass
{
    /**
     * @var string
     */
    protected $path = 'sales-invoices';
}
