<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI\Requests;

class PurchaseInvoices extends AbstractRequestClass
{
    /**
     * @var string
     */
    protected $path = 'purchase-invoices';
}
