<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI\Requests;

class Contacts extends AbstractRequestClass
{
    /**
     * @var string
     */
    protected $path = 'contacts';
}
