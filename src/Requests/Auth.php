<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI\Requests;

class Auth extends AbstractRequestClass
{
    /**
     * @param  array  $options
     * @return mixed|\Psr\Http\Message\ResponseInterface|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function user(array $options = [])
    {
        return $this->request('get', 'auth/user', $options);
    }
}
