<?php

declare(strict_types=1);

namespace Sekizbit\EDonusumAPI\Requests;

class Products extends AbstractRequestClass
{
    /**
     * @var string
     */
    protected $path = 'products';
}
