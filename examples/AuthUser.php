<?php

use Sekizbit\EDonusumAPI\Client;

require './init.php';

$edonusum = Client::instance($config);

$options = ['query' => ['signed_login_url' => 1]];

print_r($edonusum->auth()->user($options));
