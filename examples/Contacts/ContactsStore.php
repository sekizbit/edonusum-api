<?php

use Sekizbit\EDonusumAPI\Enums\ContactAccountType;
use Sekizbit\EDonusumAPI\Enums\ContactLegalType;
use Sekizbit\EDonusumAPI\Client;

require './../init.php';
$faker = Faker\Factory::create('tr_TR');

$edonusum = Client::instance($config);

$options = [
    'json' => [
        'account_type_id'   => ContactAccountType::CUSTOMER,
        'legal_type_id'     => ContactLegalType::PERSON,
        // 'legal_type_id'     => ContactLegalType::Corporation,
        'legal_name'        => $faker->name,
        'tax_number'        => $faker->tcNo,
        'tax_office'        => $faker->city,
        'emails'            => [$faker->email],
        'country_code'      => 'TR', //ISO 3166-1 Alpha-2
        'city_name'         => $faker->city,
        'district_name'     => $faker->streetName,
        'address'           => $faker->address,
        'abroad'            => false,
        'phone_number'      => $faker->e164PhoneNumber,
        'custom_attributes' => [
            ['key' => 'my_unique_id', 'value' => $faker->randomNumber(8)],
        ],
    ],
];

print_r($edonusum->contacts()->store($options));
