<?php

use Sekizbit\EDonusumAPI\Client;

require './../init.php';
$faker = Faker\Factory::create('tr_TR');

$edonusum = Client::instance($config);

$options = [
    'json' => [
        'emails'            => [$faker->email],
        'custom_attributes' => [
            ['key' => 'my_unique_id', 'value' => $faker->randomNumber(8)],
        ],
    ],
];

print_r($edonusum->contacts()->update(40, $options));
