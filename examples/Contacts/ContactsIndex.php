<?php

use Sekizbit\EDonusumAPI\Client;

require './../init.php';

$edonusum = Client::instance($config);

$options = [
    'query' => [
        'limit'      => 1000,
        'relations'  => [], //invoices, tags, city, district,
        'tax_number' => null,
        'legal_name' => null,
    ],
];

print_r($edonusum->contacts()->index($options));
