<?php

use Sekizbit\EDonusumAPI\Client;

require './../init.php';

$edonusum = Client::instance($config);

$options = [
    'query' => [
        'relations' => ['tags', 'city', 'district'],
    ],
];

print_r($edonusum->contacts()->show(953, $options));
