<?php

use Sekizbit\EDonusumAPI\Client;

require './../init.php';

$edonusum = Client::instance($config);

$options = [
    'query' => [
        'relations' => ['tags'],
    ],
];

print_r($edonusum->products()->show(4525, $options));
