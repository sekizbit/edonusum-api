<?php

use Sekizbit\EDonusumAPI\Enums\Currency;
use Sekizbit\EDonusumAPI\Client;

require './../init.php';
$faker = Faker\Factory::create('tr_TR');

$edonusum = Client::instance($config);

$options = [
    'json' => [
        'name'                       => $faker->sentence(3),
        'code'                       => $faker->randomNumber(5),
        'barcode'                    => $faker->isbn10,
        'sales_price'                => $faker->numberBetween(100, 10000),
        'sales_price_currency_id'    => Currency::TRY,
        'purchase_price'             => $faker->numberBetween(100, 10000),
        'purchase_price_currency_id' => Currency::TRY,
        'vat_rate'                   => 18,
        'custom_attributes'          => [
            ['key' => 'my_unique_id', 'value' => (string) $faker->randomNumber(8)],
        ],
    ],
];

print_r($edonusum->products()->store($options));
