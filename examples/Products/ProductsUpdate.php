<?php

use Sekizbit\EDonusumAPI\Client;

require './../init.php';
$faker = Faker\Factory::create('tr_TR');

$edonusum = Client::instance($config);

$options = [
    'json' => [
        'sales_price'       => $faker->numberBetween(100, 10000),
        'custom_attributes' => [
            ['key' => 'my_unique_id', 'value' => $faker->randomNumber(8)],
        ],
    ],
];

print_r($edonusum->products()->update(1465, $options));
