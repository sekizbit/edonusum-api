<?php

use Sekizbit\EDonusumAPI\Client;

require './../init.php';

$edonusum = Client::instance($config);

$options = [
    'query' => [
        'limit'             => 1000,
        'relations'         => [], // tags
        'keyword'           => null,
        'custom_attributes' => [
            // ['key' => 'my_unique_id', 'value' => 63150362],
        ],
    ],
];

print_r($edonusum->products()->index($options));
