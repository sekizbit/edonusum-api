<?php

use Sekizbit\EDonusumAPI\Enums\Currency;
use Sekizbit\EDonusumAPI\Enums\EInvoiceScenario;
use Sekizbit\EDonusumAPI\Enums\InvoiceDocumentType;
use Sekizbit\EDonusumAPI\Enums\InvoiceStatus;
use Sekizbit\EDonusumAPI\Enums\InvoiceSubType;
use Sekizbit\EDonusumAPI\Client;

require './../init.php';
$faker = Faker\Factory::create('tr_TR');

$edonusum = Client::instance($config);

$options = [
    'json' => [
        'sub_type_id'       => InvoiceSubType::SATIS,
        'document_type_id'  => InvoiceDocumentType::INVOICE,
        'scenario_id'       => EInvoiceScenario::EARSIVFATURA,
        // 'document_type_id'  => InvoiceDocumentType::SEVOUCHER,
        // 'scenario_id'       => EInvoiceScenario::EARSIVBELGE,
        'status_id'         => InvoiceStatus::DRAFT,
        'currency_id'       => Currency::TRY,
        'issue_date'        => date('Y-m-d'),
        'description'       => $faker->words('3', true),
        'contact_id'        => 153,
        // contact_emails gönderilmesi zorunlu değil, ancak gönderilirse bir array olarak gönderilmeli
        // 'contact_emails'    => ['deneme1@sekizbit.com.tr', 'deneme2@gmail.com'],
        'details'           => [
            [
                'product_id' => 34,
                'quantity'   => 2,
                'unit_price' => 200,
                'vat_rate'   => 18,
            ],
        ],
        'custom_attributes' => [
            ['key' => 'my_unique_id', 'value' => $faker->randomNumber(8)],
        ],
    ],
];

print_r($edonusum->salesInvoices()->store($options));
