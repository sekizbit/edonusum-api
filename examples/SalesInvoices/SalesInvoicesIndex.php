<?php

use Sekizbit\EDonusumAPI\Client;

require './../init.php';

$edonusum = Client::instance($config);

$options = [
    'query' => [
        'limit'     => 1000,
        'relations' => [], // contact, tags, details, details.product, einvoice, sentEmails
        'keyword'   => null,
    ],
];

print_r($edonusum->salesInvoices()->index($options));
